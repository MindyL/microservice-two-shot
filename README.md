# Wardrobify

Team:

* Mindy - Shoes
* Shelton - Hats

## Design
This Wardrobify app is designed for users to provide a simple solution for managing hats and shoes on the wardrobe and decide daily outfit on the go.

## Shoes microservice

In this Shoes microservice, you can:
- view all the shoes that you've stored, with detailed info such as model name, manufacturer, color, picture, and where they are stored in the closet; 
- add new shoes to your list by filling out the shoes form;
- and remove shoes that you no longer want to keep from your shoes list.

## Hats microservice

In this Hats microservice, you can:
- view all the hats that you've stored, with detailed info such as model name, style, color, picture, and where they are stored in the closet; 
- add new hats to your list by filling out the hats form;
- and remove hats that you no longer want to keep from your hats list.
