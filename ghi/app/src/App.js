import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './Hats/HatsPage'
import NewHatForm from './Hats/NewHatForm';
import ShoesForm from './Shoes/ShoesForm';
import ShoesMainPage from './Shoes/ShoesPage';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatForm />} />
          <Route path="/new_hat" element={<NewHatForm />} />
          <Route path="/shoes" element={<ShoesMainPage />} />
          <Route path="/new_shoes" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
