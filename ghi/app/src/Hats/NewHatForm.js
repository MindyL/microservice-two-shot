import React from 'react';

class NewHatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            color: '',
            fabric: '',
            style: '',
            picture_url: '',
            locations: [],
        }
        
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            
            const cleared = {
                name: '',
                color: '',
                fabric: '',
                style: '',
                picture_url: '',
                location: '',

            };
            this.setState(cleared);

        const loadingMessate = document.getElementById('success-message');
        loadingMessate.classList.remove("d-none");
        }
        else {
            const loadingMessate = document.getElementById('danger-message');
            loadingMessate.classList.remove("d-none");
        }
      }


    handleFieldChange(event) {
        const value = event.target.value;
        this.setState({[event.target.id]: value})
    }


    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        };
    }

    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Store a new Hat</h1>
                    <form onSubmit={this.handleSubmit} id="create-hat-form">

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                        <label htmlFor="color">Color</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={this.state.fabric}/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" value={this.state.style}/>
                        <label htmlFor="style">Style</label>
                    </div>

                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroup-sizing-default">Picture URL</span>
                        </div>
                        <input className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" onChange={this.handleFieldChange} type="text" name="picture_url" id="picture_url" value={this.state.picture_url}/>
                    </div>
                
                    <div className="mb-3">
                        <select onChange={this.handleFieldChange} required name="location" id="location" className="form-select" value={this.state.location}>
                            <option value="">Choose a Location</option>
                            {this.state.locations.map(location => {
                                return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Store</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                        Congratulations! Your hat has been stored!
                    </div>
                    <div className="alert alert-danger d-none mb-0" id="danger-message">
                        We are sorry ... please try again!
                    </div>
                </div>
              </div>
            </div>
        )
    }

}

export default NewHatForm;