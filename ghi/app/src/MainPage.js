import * as React from "react";
import { Link } from "react-router-dom";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <div className="row">

          <div className="col shoes">
            <div className="card mb-3 shadow">
              <Link to="/shoes"><img src="https://images.pexels.com/photos/2918534/pexels-photo-2918534.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" height="480px" className="card-img-top mainPicture" alt="This is a discription"/></Link>
              <div className="card-body mainpageBotton">
                <h4 className="card-title">SHOSE</h4>
              </div>
            </div>
          </div>
      
    
          <div className="col hats">
            <div className="card mb-3 shadow">
              <Link to="/hats"><img src="https://c8.alamy.com/comp/HFT1YM/lots-of-mens-hats-for-sale-in-a-shop-HFT1YM.jpg" className="card-img-top mainPicture" height="480px" alt="This is a discription"/></Link>
              <div className="card-body mainpageBotton">
                <h4 className="card-title">HATS</h4>
              </div>
            </div>
          </div>
      
        </div>
      </div>
    </div>
  );
}

export default MainPage;
