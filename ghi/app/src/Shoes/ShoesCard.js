function ShoesColumn(props) {

    const handleDelete = async shoesUrl => {
        const url = "http://localhost:8080" + shoesUrl;

        const fetchConfig = {
            method: "delete",
        };
    

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.reload()
            //window.location.reload() built-in function,, dom't change it!
        };
    }

    return (
        <div className="col">
            {props.list.map(data => {
                //console.log(data)
                return (
                    <div key={data.href} className="card mb-3 shadow">
                        <img src={data.picture_url} className="card-img-top" alt="This is a discription"/>
                        <div className="card-body">
                            <h5 className="card-title">{data.model_name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {data.manufacturer}
                            </h6>
                            <p className="card-text">
                                Color: {data.color}
                            </p>
                        </div>
                        <div className="card-footer">
                            Closet: {data.shoes_bin.closet_name}
                            <div onClick={ () => {handleDelete(data.href) }} className="float-end">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash3-fill svgicon" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                            </svg>  
                            </div>  
                        </div>
                    </div>
                );
            })}
        </div>
    )
}

export default ShoesColumn;