from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["id", "model_name"]

    

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "shoes_bin",
    ]

    encoders = {"shoes_bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(shoes_bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
            bin_href = content["shoes_bin"]
            shoes_bin = BinVO.objects.get(import_href=bin_href)
            content["shoes_bin"] = shoes_bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )   
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )     


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, pk):
    shoes = Shoes.objects.get(id=pk)
    if request.method == "GET":
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete() 
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        if "shoes_bin" in content:
            try:
                shoes_bin = BinVO.objects.get(id=content["shoes_bin"])
                content["shoes_bin"] = shoes_bin
            except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid bin id"},
                    status=400,
                ) 
            
        Shoes.objects.filter(id=pk).update(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )  



